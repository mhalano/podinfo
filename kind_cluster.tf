terraform {
  required_providers {
    kind = {
      # Com a versão mais nova do KinD, 0.17.0, este provider precisa ser usado no lugar do provider da kyma-incubator que está marcado como deprecated
      source  = "tehcyx/kind"
      version = "0.0.15"
    }
  }
}

provider "kind" {}

resource "kind_cluster" "default" {
  name  = "desafio"

}
