# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/tehcyx/kind" {
  version     = "0.0.15"
  constraints = "0.0.15"
  hashes = [
    "h1:tb+xaIXpYZodyagE+Oa+XcFxE5Du2MU9k9ReNJbNius=",
    "zh:02ab0e18f0bacb67c3a223043f6988ef540e4c8b34e4c629f39c1da929dac474",
    "zh:28081186a515ca07d62331a7f0715bb0aadf892c277623851e63bd8f7038ad98",
    "zh:78c35927ce6f0b855d1eb46afc5f204dd5df91723c1033bbcd239ba1d665156b",
    "zh:7a9c7645b49e7824b126c2701ed9499adfdeeb608decd19b6da0b95206fb7037",
    "zh:9b6d258af1b6c98b9faa794e3b590517c6d7e72b59e211eea98077f7fa5953a0",
    "zh:e8d25409d271cfd4588fc02e9362e6d25c965555a07df02815778fa4c87d1855",
  ]
}
